import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  DragData: any = {};
  submolecules: any[];
  subMolecule = false;
  selectedSubMolecule: any;
  Molecule = false;
  searchValue = '';
  noData = false;

  constructor(private homeservice: HomeService) {
    this.submolecules = [];
  }
  clearFilter() {
    this.submolecules = [];
    this.selectedSubMolecule = {};
    this.Molecule = false;
    this.searchValue = '';
    this.subMolecule = false;
    this.noData = false;
  }

  arrayToDropdown(InputArray) {
    let DropDownArray = [];
    InputArray.forEach(DropdownValues => {
      let dropDownObj = {};
      dropDownObj['name'] = DropdownValues;
      dropDownObj['code'] = DropdownValues;
      DropDownArray.push(dropDownObj);
    });
    return DropDownArray
  }


  ngOnInit() {
  }

  searchMolecule() {
    this.submolecules = [];
    this.homeservice.getNdaData(this.searchValue).subscribe((ndaValues) => {
      console.log("Dropdown", ndaValues['data'].nda_numbers)
      if (ndaValues['data'].nda_numbers.length > 1) {
        let dropdownvalues = this.arrayToDropdown(ndaValues['data'].nda_numbers);
        this.submolecules = dropdownvalues;
        this.subMolecule = true;
        this.Molecule = false;
        this.DragData = {};
      } else {
        this.selectedSubMolecule = {};
        this.subMolecule = false;
        this.Molecule = false;
        this.DragData = {};
      }
    })
  }

  getData() {
    if (this.selectedSubMolecule !== undefined) {
      let ndaValue = this.selectedSubMolecule['code'];
      this.homeservice.getDrugData(ndaValue).subscribe((MoleculeData) => {
        if (MoleculeData !== null) {
          console.log("data", MoleculeData);
          this.Molecule = true;
          this.DragData = MoleculeData;
          this.noData = false;
        } else {
          this.noData = true;
          this.Molecule = false;
          this.DragData = {};
        }

      })
    }
  }

}
