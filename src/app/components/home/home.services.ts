import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { AppConstants } from '../../app.constants';

@Injectable()
export class HomeService {

    constructor(private http: HttpClient) { }

    getNdaData(searchValue): Observable<any[]> {
        let paramsInput = new HttpParams();
        paramsInput = paramsInput.append('brand_name', searchValue);
        return this.http.get<any[]>(`${AppConstants.API_ENDPOINT}${AppConstants.API.MOLECULE.GET}`, { params: paramsInput })
    }

    getDrugData(ndaValue): Observable<any[]> {
        let paramsInput = new HttpParams();
        paramsInput = paramsInput.append('nda', ndaValue);
        return this.http.get<any[]>(`${AppConstants.API_ENDPOINT}${AppConstants.API.DETAILS.GET}`, { params: paramsInput })
    }

}