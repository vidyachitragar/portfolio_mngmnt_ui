import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  loginForm!: FormGroup;        // login form variable declaration to hold form values 
  submitted = false;
  invalidCredentials = false;

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    localStorage.clear();
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],                 // email validations 
      password: ['', [Validators.required, Validators.minLength(6)]],       // password with 6 characters 
    }, {

    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }


  onSubmit() {
    this.submitted = true;
    
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.postLoginDetails(this.loginForm.value.email, this.loginForm.value.password)
      .subscribe((data) => {
        console.log("data", data);
        if (data.status === 200) {
          localStorage.setItem("email", this.loginForm.value.email);
          this.router.navigateByUrl('/home');
          this.invalidCredentials = false;
        } else {
          this.invalidCredentials = true;
        }

      },
        (err) => {
          this.invalidCredentials = true;
        });
  }

  onReset() {
    this.submitted = false;
    this.loginForm.reset();
  }
}
