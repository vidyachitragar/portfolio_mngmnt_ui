import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { AppConstants } from '../../app.constants';



@Injectable()
export class AuthService {
    constructor(private http: HttpClient) { }

    postLoginDetails(email: string, password: string): Observable<any> {
        let body = {
            "email": email,
            "password": password,
            "remember": true
        }
        if (email === 'admin@pms.com' && password === 'admin123') {
            return of({ status: 200, email: 'admin' });
            // return this.http.post<any[]>(`${AppConstants.API_ENDPOINT}${AppConstants.API.LOGIN.POST_LOGIN}`,
            // body);
        } else {
            return of({ status: 404 });
        }

    }



}
