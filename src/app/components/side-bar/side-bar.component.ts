import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss']
})
export class SideBarComponent implements OnInit {

  sidebarItems: any[];
  isSideBarOpen = false;
  activeIndex: number = null;
  userName = '';
  name: any;
  isAdmin = false;
  profile;
  constructor(private router: Router, private http: HttpClient) {
    this.sidebarItems = [
      { title: 'Home', icons: 'fa fa-home', path: '/home' },
      { title: 'Log Out', icons: 'fa fa-sign-out', path: '/login' },
    ];
  }

  ngOnInit(): void {
    //add this for AD
    this.sidebarItems.map((eachItem, ind) => {
      if (eachItem.path === this.router.url) {
        this.activeIndex = ind;
      }
    });
    setTimeout(() => {
      let userDetails = localStorage.getItem("user_data");
      userDetails = JSON.parse(userDetails)
      this.userName = 'admin';
      this.sidebarItems = [
        { title: 'Home', icons: 'fa fa-home', path: '/home' },
        { title: 'Log Out', icons: 'fa fa-sign-out', path: '/login' },
      ];
    }, 1000);


  }

  selecteByRoute(selectedItem, ind): any {
    this.sidebarItems.map(eachItem => {
      if (eachItem.title === selectedItem.title) {
        this.activeIndex = ind;
        this.router.navigate([`${selectedItem.path}`]);
      }
    });
  }

  logout() {
    localStorage.clear();
    sessionStorage.clear();
  }
}
