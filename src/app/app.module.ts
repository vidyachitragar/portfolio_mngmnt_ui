import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SideBarComponent } from './components/side-bar/side-bar.component';
import { AuthComponent } from './components/auth/auth.component';
import { HomeComponent } from './components/home/home.component';
import { AuthService } from './components/auth/auth.service';
import { DropdownModule } from 'primeng/dropdown';
import { HomeService } from './components/home/home.services';
import { NgHttpLoaderModule } from 'ng-http-loader'; 

import {
  ObjecttoKeyvalue, ArrayChk, TableChk,
  TableHeaders, TableBody
} from './core/utils/custompipes';


@NgModule({
  declarations: [
    AppComponent,
    SideBarComponent,
    AuthComponent,
    HomeComponent,
    ObjecttoKeyvalue,
    ArrayChk,
    TableChk,
    TableHeaders,
    TableBody
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    DropdownModule,
    NgHttpLoaderModule.forRoot()
  ],
  providers: [AuthService, HomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
