const server = {
    endPoint: 'http://15.206.108.189:8000/',
};

export const AppConstants = {
    API_ENDPOINT: server.endPoint,
    API: {
        LOGIN: {
            POST_LOGIN: 'login',
        },
        MOLECULE: {
            GET: 'info/get_nda/'
        },
        DETAILS: {
            GET: 'info/fetch_molecule_details/'
        }
    }
};
