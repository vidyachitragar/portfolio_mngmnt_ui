import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';

@Injectable()
export class AppService {
  isRequestLoading = false;

  get user(): any {
    return this.authService.user;
  }

  get userChanged(): BehaviorSubject<any> {
    return this.authService.userChanged;
  }

  get isLoggedIn(): boolean {
    return this.user != null && this.loginChecked;
  }

  get loginChecked(): boolean {
    return this.authService.loginChecked;
  }

  constructor(public authService: AuthService) {}
}
