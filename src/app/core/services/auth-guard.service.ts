import { Observable } from 'rxjs';

import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanLoad, Route, Router } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuardService implements CanActivate, CanLoad {
  constructor(public router: Router, private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot): boolean | Observable<boolean> {
    return this.checkAuthentication();
  }

  canLoad(route: Route): boolean | Observable<boolean> {
    return this.checkAuthentication(;
  }

  checkAuthentication(): boolean | Observable<boolean> {
    return this.authService.isLoggedInAsync;
  }
}
