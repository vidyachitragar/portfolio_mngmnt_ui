import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'Object2keyvalue' })
export class ObjecttoKeyvalue implements PipeTransform {
    transform(Inputvalue: any): any {
        let newArray = [];
        Object.entries(Inputvalue).forEach(
            ([key, value]) => {
                let newObj = {}
                newObj['key'] = key;
                newObj['value'] = value;
                newArray.push(newObj)
            }
        );
        return newArray;
    }
}


@Pipe({ name: 'arrayChk' })
export class ArrayChk implements PipeTransform {
    transform(Inputvalue: any): any {
        if (Array.isArray(Inputvalue)) {
            return true;
        } else {
            return false;
        }
    }
}


@Pipe({ name: 'tableChk' })
export class TableChk implements PipeTransform {
    transform(Inputvalue: any): any {
        if (typeof Inputvalue[0] === 'object') {
            return true;
        } else {
            return false;
        }
    }
}

@Pipe({ name: 'tableHeaders' })
export class TableHeaders implements PipeTransform {
    transform(Inputvalue: any): any {
        let newArray = [];
        Object.entries(Inputvalue[0]).forEach(
            ([key, value]) => {
                newArray.push(key)
            }
        );
        return newArray;
    }
}

@Pipe({ name: 'tableBody' })
export class TableBody implements PipeTransform {
    transform(Inputvalue: any): any {
        let newArray = [];
        Inputvalue.forEach(element => {
            let innerArray = []
            Object.entries(element).forEach(
                ([key, value]) => {
                    innerArray.push(value)
                }
            );
            newArray.push(innerArray);
        });
        return newArray;
    }
}